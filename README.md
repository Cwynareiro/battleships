## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [How to play](#how-to-play)

## General info
This project is a recreation of the board game called Battleships. I made it in order to practise algorithms and object-oriented programming. In this game you play against an artificial opponent who makes his choices randomly.
	
## Technologies
Project is created with:
* Java
* Maven
	
## How to play
The game is played in your IDE's console and has the same rules as the original Battleships game. In the beginning you have to choose board sizes and the number of ships each player has in play, than you have to specify in which spaces u want to place your ships. After placing your ships the actual game begins. In each round both you and your opponent try to shoot down an enemy ship. The game ends after one of the players shoots down all the enemy ships. 
### How to make choices
In order to choose a space in any stage of the game you have to input two digits representing that space and press "enter".
### Board stats explanation
Enemy board

| Symbol  | What it means |
| ------------- | ------------- |
| [S] | Space with a sunk ship |
| [x] | Space that you tried to shoot but it didn't have a ship  |

Your board

| Symbol  | What it means |
| ------------- | ------------- |
| [T] | Space with a sunk ship |
| [x] | Space that enemy tried to shoot but it didn't have a ship  |
| [S] | Space that has a ship but was not chosen by the enemy yet  |