import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Player {
    private String name;
    private BattleShipsBoardStatus boardStatus;

    public Player(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBoardStatus(BattleShipsBoardStatus boardStatus) {
        this.boardStatus = boardStatus;
    }

    public String getName() {
        return name;
    }

    public BattleShipsBoardStatus getBoardStatus() {
        return boardStatus;
    }

    void placeAShip(){
        Scanner spaceVariables = new Scanner(System.in);
        int x=spaceVariables.nextInt();
        int y=spaceVariables.nextInt();
        x=x-1;
        y=y-1;
        while(x>=boardStatus.getPlayerBoard().getX() || y>=boardStatus.getPlayerBoard().getY() || x<0 || y<0)
        {
            System.out.println("The space in which you tried to place a ship does not exist, try again");
            x = spaceVariables.nextInt();
            y = spaceVariables.nextInt();
            x = x - 1;
            y = y - 1;
        }
        boardStatus.getPlayerBoard().getSpaces().get(x).get(y).setHasAShip(true);
    }
    void computerPlacesAShip(){
        int x;
        int y;
        Random generator = new Random();
        x=generator.nextInt(getBoardStatus().getEnemyBoard().getX());
        y=generator.nextInt(getBoardStatus().getEnemyBoard().getY());
        while (boardStatus.getEnemyBoard().getSpaces().get(x).get(y).isHasAShip()==true) {
            x = generator.nextInt(getBoardStatus().getEnemyBoard().getX());
            y = generator.nextInt(getBoardStatus().getEnemyBoard().getY());
        }

        boardStatus.getEnemyBoard().getSpaces().get(x).get(y).setHasAShip(true);
    }
    void computerRandomGuess (){
        int x;
        int y;
        Random generator = new Random();
        x=generator.nextInt(getBoardStatus().getPlayerBoard().getX());
        y=generator.nextInt(getBoardStatus().getPlayerBoard().getY());
        while (boardStatus.getPlayerBoard().getSpaces().get(x).get(y).isWasChosen()==true) {
            x = generator.nextInt(getBoardStatus().getPlayerBoard().getX());
            y = generator.nextInt(getBoardStatus().getPlayerBoard().getY());
        }

        boardStatus.getPlayerBoard().getSpaces().get(x).get(y).setWasChosen(true);
        if (boardStatus.getPlayerBoard().getSpaces().get(x).get(y).isHasAShip() == true) {
            System.out.println("Enemy has hit your ship on space x:" + (x + 1) + " y:" + (y + 1));
            boardStatus.getPlayerBoard().setSpacesWithShipsNumber(boardStatus.getPlayerBoard().getSpacesWithShipsNumber() - 1);
        }
        else
            System.out.println("Enemy tried to shoot space x:" + (x+1) + "y:" + (y+1) +" but nothing was there!");

    }
    void makeAGuess(int x, int y){
        x=x-1;
        y=y-1;
        boardStatus.getEnemyBoard().getSpaces().get(x).get(y).setWasChosen(true);
        if (boardStatus.getEnemyBoard().getSpaces().get(x).get(y).isHasAShip() == true){
            System.out.println("You hit a ship");
            boardStatus.getEnemyBoard().setSpacesWithShipsNumber(boardStatus.getEnemyBoard().getSpacesWithShipsNumber()-1);
        }
        else
            System.out.println("Nothing was there : (");


    }
}
