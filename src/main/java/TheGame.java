import java.util.Scanner;

public class TheGame {
    Player player= new Player("Player");
    BattleShipsBoardStatus status= new BattleShipsBoardStatus();
    int round=1;

    public TheGame() {

    }

    void gameStart(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to battleships");

        String boardName = "Players board";
        System.out.println("How big board do you want?");
        int boardSize=sc.nextInt();
        while(boardSize<0)
        {
            System.out.println("Board cannot have a negative size, try again");
            boardSize=sc.nextInt();
        }
        System.out.println("How many ships do you want?");
        int shipsNumber=sc.nextInt();
        while (shipsNumber<0 || Math.pow(boardSize,2)-1<shipsNumber)
        {
            System.out.println("The numeber of ships must me positive and smaller than the number of available spaces, try again");
            shipsNumber=sc.nextInt();
        }
        Board playersBoard = new Board(boardSize,boardSize,shipsNumber);
        Board computersBoard = new Board(boardSize,boardSize,shipsNumber);
        status.setPlayerBoard(playersBoard);
        status.setEnemyBoard(computersBoard);
        player.setBoardStatus(status);
        System.out.println("Setup your ships");
        for(int i=0;i<shipsNumber;i++) {
            System.out.println("Ship number "+(i+1));
            player.placeAShip();
            player.computerPlacesAShip();
        }
        System.out.println("\n\n\n\nLets begin!!!");
        round();
    }
    void gameEnd(){
        System.out.println("\n___The end___\n");
        if (status.getPlayerBoard().getSpacesWithShipsNumber()==0)
            System.out.println("Enemy wins");
        else
            System.out.println("You win!" );

        System.exit(0);
    }
    void shootingSequence(){
        Scanner spaceVariables = new Scanner(System.in);
        int x=spaceVariables.nextInt();
        int y=spaceVariables.nextInt();
        while(x>status.getEnemyBoard().getX() || y>status.getEnemyBoard().getY() || x<0 || y<0)
        {
            System.out.println("The space u tried to shoot does not exist, try again");
            x = spaceVariables.nextInt();
            y = spaceVariables.nextInt();
        }
        player.makeAGuess(x,y);
        player.computerRandomGuess();
        round++;

        if(status.getEnemyBoard().getSpacesWithShipsNumber()==0 || status.getPlayerBoard().getSpacesWithShipsNumber()==0)
            gameEnd();




    }
    public void run() {

        TheGame gra = new TheGame();
        gra.gameStart();
    }
    void round(){

        int decision=0;

        while (decision!=10){

            System.out.println("\n\n---Round number "+round+"---\n"+
                    "---What do you want to do?---\n"+
                    "1. Show me ENEMY board state\n"+
                    "2. Show me MY board state\n"+
                    "3. Shoot\n"+
                    "10. End game ");
            Scanner whatToDo = new Scanner(System.in);
            decision=whatToDo.nextInt();
            switch (decision) {
                default:
                    System.out.println("\n\n\nYou entered incorrect number\n");
                    break;
                case 1:
                    System.out.println("Enemy board:");
                    status.getEnemyBoard().ShowCurrentBoard(false);
                    break;
                case 2:
                    System.out.println("Your board:");
                    status.getPlayerBoard().ShowCurrentBoard(true);
                    break;
                case 3:
                    shootingSequence();
                    break;
                case 10:
                    decision=10;

            }
        }
    }
}