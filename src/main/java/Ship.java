import java.util.ArrayList;
import java.util.List;

public class Ship {
    private List<Space> spaces= new ArrayList<Space>();
    private boolean isSunk;

    public List<Space> getSpaces() {
        return spaces;
    }

    public void setSpaces(List<Space> spaces) {
        this.spaces = spaces;
    }

    public boolean isSunk() {
        return isSunk;
    }

    public void setSunk(boolean sunk) {
        isSunk = sunk;
    }
}
