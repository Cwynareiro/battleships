public class Space {
    private boolean wasChosen;
    private boolean hasAShip;

    public Space(){
        this.wasChosen=false;
        this.hasAShip=false;
    }

    @Override
    public String toString() {
        return "Space{" +

                ", wasChosen=" + wasChosen +
                ", hasAShip=" + hasAShip +
                '}';
    }

    public boolean isWasChosen() {
        return wasChosen;
    }

    public void setWasChosen(boolean wasChosen) {
        this.wasChosen = wasChosen;
    }

    public boolean isHasAShip() {
        return hasAShip;
    }

    public void setHasAShip(boolean hasAShip) {
        this.hasAShip = hasAShip;
    }
}
