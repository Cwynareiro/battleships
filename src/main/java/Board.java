import java.util.ArrayList;
import java.util.List;

public class Board {



    private List<List<Space>> spaces= new ArrayList<List<Space>>();
    private List<Ship> ships= new ArrayList<Ship>();
    private int x;
    private int y;
    private int spacesWithShipsNumber;

    public int getSpacesWithShipsNumber() {
        return spacesWithShipsNumber;
    }

    public void setSpacesWithShipsNumber(int spacesWithShipsNumber) {
        this.spacesWithShipsNumber = spacesWithShipsNumber;
    }

    public Board(int x, int y, int spacesWithShipsNumber) {

        this.x=x;
        this.y=y;
        this.spacesWithShipsNumber=spacesWithShipsNumber;
        for (int i=1; i<x +1; i++) {
            List<Space> col = new ArrayList<Space>();
            for (int j = 1; j < y +1; j++)
        col.add(new Space());
            spaces.add(col);
        }
    }


    @Override
    public String toString() {
        return "Board{" +
                "spaces=" + spaces +
                ", ships=" + ships +
                ", x=" + x +
                ", y=" + y +
                ", spacesWithShipsNumber=" + spacesWithShipsNumber +
                '}';
    }

    public List<List<Space>> getSpaces() {
        return spaces;
    }

    public void setSpaces(List<List<Space>> spaces) {
        this.spaces = spaces;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }

    void ShowCurrentBoard(boolean access) {
        for (int i = 1; i < x+1; i++)
            System.out.print("  "+i+"");
        System.out.println();
        if (access == false){
             for (int i=1; i<y+1; i++)
            {
                System.out.print(i);
                for (int j=1; j<x+1 ;j++) {
                    if (spaces.get(j-1).get(i-1).isHasAShip() == true && spaces.get(j-1).get(i-1).isWasChosen() == true)
                        System.out.print("[S]");
                    else
                    if (spaces.get(j-1).get(i-1).isWasChosen() == true)
                        System.out.print("[x]");
                    else
                        System.out.print("[ ]");
                }
                System.out.println();
            }
        }
        else
            for (int i=1; i<y+1; i++)
            {
                System.out.print(i);
                for (int j=1; j<x+1 ;j++) {
                    if (spaces.get(j-1).get(i-1).isHasAShip() == true && spaces.get(j-1).get(i-1).isWasChosen() == true)
                        System.out.print("[T]");
                    else
                    if (spaces.get(j-1).get(i-1).isHasAShip() == false &&spaces.get(j-1).get(i-1).isWasChosen() == true)
                        System.out.print("[x]");
                    else
                    if (spaces.get(j-1).get(i-1).isHasAShip() == true && spaces.get(j-1).get(i-1).isWasChosen() == false)
                        System.out.print("[S]");
                    else
                        System.out.print("[ ]");
                }
                System.out.println();
            }
    }

}
